//
//  DesignableTextField.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/8/21.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField
{
    @IBInspectable var cornerRadius:CGFloat = 0
    {
        didSet
        {
            layer.cornerRadius = cornerRadius
        }
    }
    
    //for left view
    @IBInspectable var leftImage: UIImage?
    {
        didSet
        {
           updateLeftView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    {
        didSet
        {
            updateLeftView()
        }
    }
 
    //for right view
    @IBInspectable var rightImage: UIImage?
    {
        didSet
        {
           updateRightView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = -12
    {
        didSet
        {
            updateRightView()
        }
    }
    
    //updating left view
    func updateLeftView()
    {
        if let image = leftImage
        {
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 25, height: 20))
            imageView.image = image
           
            var width = leftPadding + 20
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line
            {
                width = width + 10
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
            leftView = view
        }
        else
        {
            //leftImage is nill
            leftViewMode = .never
        }
    }
    
    //updating right view
    func updateRightView()
    {
        if let image = rightImage
        {
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: rightPadding, y: 0, width: 25, height: 15))
            imageView.image = image
           
            var width = rightPadding - 20
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line
            {
                width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 15))
            view.addSubview(imageView)
            
            rightView = view
        }
        else
        {
            //leftImage is nill
            rightViewMode = .never
        }
    }
    
}
