//
//  PasswordResetViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/3/21.
//

import UIKit

class PasswordResetViewController: UIViewController
{
    //UIObjects
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    
    @IBAction func backBtn(_ sender: UIButton)
    {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var btnReset: UIButton!
    @IBAction func btnResetAction(_ sender: UIButton)
    {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClaimsHomeViewController") as! ClaimsHomeViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnReset.layer.cornerRadius = 10
    }
}

