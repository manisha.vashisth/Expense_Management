//
//  PasswordResetViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/3/21.
//

import UIKit

class PasswordResetViewController: UIViewController
{
    //UIObjects
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword: UITextField!
    
    @IBAction func btnBack(_ sender: UIButton)
    {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var btnReset: UIButton!
    @IBAction func btnResetAction(_ sender: UIButton)
    {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "ClaimsHomeViewController") as! ClaimsHomeViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnReset.layer.cornerRadius = 10
        
        let newPasswordImage = UIImage(systemName: "lock")
        newPasswordImage?.withTintColor(.black)
        txtFieldLeftImage(txtField: txtNewPassword, addImage: newPasswordImage!)
        
        let confirmPasswordImage = UIImage(systemName: "lock")
        confirmPasswordImage?.withTintColor(.black)
        txtFieldLeftImage(txtField: txtConfirmNewPassword, addImage: confirmPasswordImage!)
        
        let openEyeImage = UIImage(systemName: "eye")
        newPasswordImage?.withTintColor(.black)
        txtFieldRightImage(txtField: txtNewPassword, addImage: openEyeImage!)
    
        let closeEyeImage = UIImage(systemName: "eye.slash")
        newPasswordImage?.withTintColor(.black)
        txtFieldRightImage(txtField: txtConfirmNewPassword, addImage: closeEyeImage!)
    }
}

extension PasswordResetViewController
{
    func txtFieldLeftImage(txtField: UITextField, addImage img: UIImage)
    {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
    
    func txtFieldRightImage(txtField: UITextField, addImage img: UIImage)
    {
        let rightImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        rightImageView.image = img
        txtField.rightView = rightImageView
        txtField.rightViewMode = .always
    }
}
