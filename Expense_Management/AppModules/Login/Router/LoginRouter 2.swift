//
//  LoginRouter.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/4/21.
//

import Foundation
import UIKit

class LoginRouter:PresenterToRouterProtocol
{
    static func createModule() -> LoginViewController
    {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = LoginPresenter()
        let interactor: PresenterToInteractorProtocol = LoginInteractor()
        let router: PresenterToRouterProtocol = LoginRouter()
        
        view.presentor = presenter
        presenter.view = view as! PresenterToViewProtocol
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    func routeToSignIn(navigatioController: UINavigationController)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ClaimsHomeViewController") as! ClaimsHomeViewController
        navigatioController.pushViewController(vc, animated: true)
         print("Router ---- navigate")
        
    }
    
    static var mainstoryboard: UIStoryboard
    {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
