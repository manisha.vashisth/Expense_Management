//
//  LoginPresenter.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/4/21.
//

import Foundation
import UIKit

class LoginPresenter:ViewToPresenterProtocol
{
    var view: PresenterToViewProtocol?
    var interactor: PresenterToInteractorProtocol?
    var router: PresenterToRouterProtocol?
    
    func presentToSignIn(navigationController: UINavigationController)
    {
        router?.routeToSignIn(navigatioController:navigationController)
        print("presenter to router")
    }
}

extension LoginPresenter: InteractorToPresenterProtocol
{
    func usersFetchFailed()
    {
    }
}
