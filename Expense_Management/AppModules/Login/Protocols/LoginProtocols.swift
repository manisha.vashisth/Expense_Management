//
//  LoginProtocols.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/4/21.
//

import Foundation
import UIKit

protocol ViewToPresenterProtocol: class
{
    var view: PresenterToViewProtocol?{ get set }
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    
    func presentToSignIn(navigationController:UINavigationController)
}

protocol PresenterToViewProtocol: class
{
    func showError()
}

protocol PresenterToRouterProtocol: class
{
    static func createModule()-> LoginViewController
    func routeToSignIn(navigatioController: UINavigationController)
}

protocol PresenterToInteractorProtocol: class
{
    var presenter:InteractorToPresenterProtocol? {get set}
    func fetchUsers()
}

protocol InteractorToPresenterProtocol: class
{
    func usersFetchFailed()
}
