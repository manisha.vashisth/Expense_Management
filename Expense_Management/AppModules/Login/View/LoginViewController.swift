//
//  ViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/2/21.
//

import UIKit

class LoginViewController: UIViewController
{
    var presentor:ViewToPresenterProtocol?
    
    //UIObjects
    @IBOutlet weak var txtUserID: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnSignIn.layer.cornerRadius = 10
    }
    
    @IBAction func btnRememberMeAction(_ sender: UIButton)
    {
        if btnRememberMe.tag == 0
        {
            btnRememberMe.tag = 1
            btnRememberMe.setImage(UIImage(named: "blank-1"), for: .normal)
            //btnRememberMe.backgroundColor = .white
        }
        else
        {
            btnRememberMe.tag = 0
            btnRememberMe.setImage(UIImage(named: "checkmark"), for: .normal)
        }
    }
    
    @IBAction func btnForgotPassword(_ sender: UIButton)
    {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignInAction(_ sender: UIButton)
    {
      self.presentor?.presentToSignIn(navigationController: navigationController!)
    }
}

