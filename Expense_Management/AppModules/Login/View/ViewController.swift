//
//  ViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/2/21.
//

import UIKit

class LoginViewController: UIViewController
{
    //UIObjects
    @IBOutlet weak var txtUserID: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
   
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBAction func btnRememberMeAction(_ sender: UIButton)
    {
      //action
    }
    
    @IBAction func btnForgotPassword(_ sender: UIButton)
    {
      //action + navigation
    }
    
    @IBOutlet weak var btnSignIn: UIButton!
    @IBAction func btnSignInAction(_ sender: UIButton)
    {
      //actin + navigation
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnSignIn.layer.cornerRadius = 10
    }
}

