//
//  ForgotPasswordRouter.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/8/21.
//

import Foundation
import UIKit

class ForgotPasswordRouter:PresenterToForgotPasswordRouterProtocol
{
    static var mainstoryboard: UIStoryboard
    {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    static func createModule() -> ForgotPasswordViewController
    {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
       // let presenter: ViewToForgotPasswordPresenterProtocol & InteractorToForgotPasswordPresenterProtocol = ForgotPasswordPresenter()
        let interactor: PresenterToForgotPasswordInteractorProtocol = ForgotPasswordInteractor()
        let router: PresenterToForgotPasswordRouterProtocol = ForgotPasswordRouter()
        
        //view.presentor = presenter
       // presenter.view = view as! PresenterToForgotPasswordViewProtocol
       // presenter.router = router
       // presenter.interactor = interactor
       // interactor.presenter = presenter
        
        return view
    }
    
    func routeToNext(navigatioController: UINavigationController)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PasswordResetViewController") as! PasswordResetViewController
        navigatioController.pushViewController(vc, animated: true)
         print("Router ---- navigate")
    }
}
