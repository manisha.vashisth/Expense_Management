//
//  ForgotPasswordProtocols.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/8/21.
//

import Foundation
import UIKit

protocol ViewToForgotPasswordPresenterProtocol: class
{
    var view: PresenterToForgotPasswordViewProtocol?{ get set }
    var interactor: PresenterToForgotPasswordInteractorProtocol? {get set}
    var router: PresenterToForgotPasswordRouterProtocol? {get set}
    
    func presentToNext(navigationController:UINavigationController)
}

protocol PresenterToForgotPasswordViewProtocol: class
{
   // func showError()
}

protocol PresenterToForgotPasswordRouterProtocol: class
{
    static func createModule()-> ForgotPasswordViewController
    func routeToNext(navigatioController: UINavigationController)
}

protocol PresenterToForgotPasswordInteractorProtocol: class
{
    var presenter:InteractorToForgotPasswordPresenterProtocol? {get set}
   // func fetchUsers()
}

protocol InteractorToForgotPasswordPresenterProtocol: class
{
    //func usersFetchFailed()
}
