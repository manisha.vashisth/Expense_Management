//
//  ForgotPasswordViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/3/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController
{
    //UIObjects
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBAction func btnBack(_ sender: UIButton)
    {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var btnNext: UIButton!
    @IBAction func btnNextAction(_ sender: UIButton)
    {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "PasswordResetViewController") as! PasswordResetViewController
      self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnNext.layer.cornerRadius = 10
        
        let emailImage = UIImage(systemName: "mail")
        emailImage?.withTintColor(.black)
        txtFieldLeftImage(txtField: txtEmail, addImage: emailImage!)
    }
}

extension ForgotPasswordViewController
{
    func txtFieldLeftImage(txtField: UITextField, addImage img: UIImage)
    {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
}
