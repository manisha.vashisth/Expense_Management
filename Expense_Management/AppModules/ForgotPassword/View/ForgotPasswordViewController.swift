//
//  ForgotPasswordViewController.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/3/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController
{
    var presentor:ViewToForgotPasswordPresenterProtocol?
    
    //UIObjects
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnNext: UIButton!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnNext.layer.cornerRadius = 10
    }
    
    @IBAction func backBtn(_ sender: UIButton)
    {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: UIButton)
    {
      //let vc = self.storyboard?.instantiateViewController(withIdentifier: "PasswordResetViewController") as! PasswordResetViewController
      //self.navigationController?.pushViewController(vc, animated: true)
    }
}
