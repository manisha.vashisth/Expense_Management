//
//  ForgotPasswordInteractor.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/8/21.
//

import Foundation
class ForgotPasswordInteractor: PresenterToForgotPasswordInteractorProtocol
{
    var presenter: InteractorToForgotPasswordPresenterProtocol?
}
