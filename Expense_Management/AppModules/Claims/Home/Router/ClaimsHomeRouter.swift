//
//  ClaimsHomeRouter.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/5/21.
//

import Foundation
import UIKit

class ClaimsHomeRouter:PresenterToRouterClaimsHomeProtocol{
    
    static func createClaimsHomeModule() -> ClaimsHomeViewController {
        
        let view = ClaimsHomeRouter.mainstoryboard.instantiateViewController(withIdentifier: "ClaimsHomeViewController") as! ClaimsHomeViewController

        let presenter: ViewToPresenterClaimsHomeProtocol & InteractorToPresenterClaimsHomeProtocol = ClaimsHomePresenter()
        let interactor: PresenterToInteractorClaimsHomeProtocol = ClaimsHomeInteractor()
        let router:PresenterToRouterClaimsHomeProtocol = ClaimsHomeRouter()
        
        view.presenter = presenter
        presenter.view = (view as! PresenterToViewClaimsHomeProtocol)
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard
    {
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
}
