//
//  MovieProtocol.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/5/21.
//

import Foundation

protocol ViewToPresenterClaimsHomeProtocol:class{
    
    var view: PresenterToViewClaimsHomeProtocol? {get set}
    var interactor: PresenterToInteractorClaimsHomeProtocol? {get set}
    var router: PresenterToRouterClaimsHomeProtocol? {get set}
    //func startFetchingMovie()
}

protocol PresenterToViewClaimsHomeProtocol:class
{
    //func onMovieResponseSuccess(movieModelArrayList:Array<MovieModel>)
   // func onMovieResponseFailed(error:String)
}

protocol PresenterToRouterClaimsHomeProtocol:class
{
    //static func createMovieModule()->MovieViewController
}

protocol PresenterToInteractorClaimsHomeProtocol:class
{
    var presenter:InteractorToPresenterClaimsHomeProtocol? {get set}
}

protocol InteractorToPresenterClaimsHomeProtocol:class
{
}
