//
//  ClaimsHomePresenter.swift
//  Expense_Management
//
//  Created by Manisha Vashist on 8/5/21.
//

import Foundation
import UIKit

class ClaimsHomePresenter:ViewToPresenterClaimsHomeProtocol
{
    var view: PresenterToViewClaimsHomeProtocol?
    var interactor: PresenterToInteractorClaimsHomeProtocol?
    var router: PresenterToRouterClaimsHomeProtocol?
}

extension ClaimsHomePresenter: InteractorToPresenterClaimsHomeProtocol
{
}
